## Author;
## Brendon Droege
## CprE/CybE 331
## Lab 02, Part 2, 2022

from hashlib import new


message_to_decrypt = input('Message to Decrypt: ')
msgSize = len(message_to_decrypt)
print("Length: " + str(msgSize))
freqCounterDEC = input("Type 1 to find Frequency of Letters on the text provided: " + "\n" + "Type 2 if you would like permutate some letters in the text provided!: " )



def freqCounter(message):
    
    freq = {} ## Empty list to start

    for x in message:
        if x in freq:
            freq[x] += 1
        else:
            freq[x] = 1

    for y in message:
        if freq[y] != 0:
            temp = freq[y]
            freq[y] = round((freq[y] / msgSize) * 100.0, 2)
            print(y + " = " + str(freq[y]) + " (" + str(temp) + "/" + str(msgSize) + ")")
            freq[y] = 0

def permutateMe(message):
    oldCharacter = input("What letter would you like to replace? ")
    newCharacter = input("What would you like to replace " + oldCharacter + " with? ")
    oldCharacter = oldCharacter.upper()
    newCharacter = newCharacter.upper()
    message = message.replace(oldCharacter, newCharacter)
    print(message)

    oldCharacter = input("What letter would you like to replace? ")
    newCharacter = input("What would you like to replace " + oldCharacter + " with? ")
    oldCharacter = oldCharacter.upper()
    newCharacter = newCharacter.upper()
    message = message.replace(oldCharacter, newCharacter)
    print(message)

    oldCharacter = input("What letter would you like to replace? ")
    newCharacter = input("What would you like to replace " + oldCharacter + " with? ")
    oldCharacter = oldCharacter.upper()
    newCharacter = newCharacter.upper()
    message = message.replace(oldCharacter, newCharacter)
    print(message)

    oldCharacter = input("What letter would you like to replace? ")
    newCharacter = input("What would you like to replace " + oldCharacter + " with? ")
    oldCharacter = oldCharacter.upper()
    newCharacter = newCharacter.upper()
    message = message.replace(oldCharacter, newCharacter)
    print(message)

    oldCharacter = input("What letter would you like to replace? ")
    newCharacter = input("What would you like to replace " + oldCharacter + " with? ")
    oldCharacter = oldCharacter.upper()
    newCharacter = newCharacter.upper()
    message = message.replace(oldCharacter, newCharacter)
    print(message)

    oldCharacter = input("What letter would you like to replace? ")
    newCharacter = input("What would you like to replace " + oldCharacter + " with? ")
    oldCharacter = oldCharacter.upper()
    newCharacter = newCharacter.upper()
    message = message.replace(oldCharacter, newCharacter)
    print(message)

if freqCounterDEC == "1":
    freqCounter(message_to_decrypt)
elif freqCounterDEC == "2":
    permutateMe(message_to_decrypt)

    
    