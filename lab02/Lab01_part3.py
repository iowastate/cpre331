## Author;
## Brendon Droege
## CprE/CybE 331
## Lab 02, Part 3, 2022

## This Cipher is called "The Devilish Two's"

import re
import string

alphabets = (string.ascii_lowercase, string.ascii_uppercase, string.digits) ## Stolen from the internet (string library)
def shiftMethod(message, shiftBy, alphabets): ## The homemade shifter from part 01
    global messageFinal

    def shiftME(alphabet):
        return alphabet[shiftBy:] + alphabet[:shiftBy] ## How we actually attach the shifted letters together and "shift" them

    firstShift = tuple(map(shiftME, alphabets)) ## Makes an interable and mutable list

    combineSequence = ''.join(alphabets)
    completeShift = ''.join(firstShift)

    finalTable = str.maketrans(combineSequence, completeShift)

    messageFinal = message.translate(finalTable)



message_to_encrypt_ORIGINAL = input("Message to Encrypt: ") ## Take user input for the message to encrypt
message_to_encrypt = message_to_encrypt_ORIGINAL.replace(" ", "") ## Remove spaces
message_to_encrypt = message_to_encrypt.upper() ## Capitalizes every letter
message_to_encrypt = re.sub(r'[^\w\s]', '', message_to_encrypt) ## Removes punctuation (stolen from the re library)
print ("Original Message: " + message_to_encrypt_ORIGINAL)
print ("Fixed Message: " + message_to_encrypt)
print ("--------------------------------------------" + "\n")

def encryptTheMessage(message_to_encrypt): 
    ## First, flip the letters (start with the end, end with the start), and then flip every pair
    message_to_encrypt = "".join(reversed([message_to_encrypt[x:x+2] for x in range(0, len(message_to_encrypt), 2)]))
    ## print ("Message after the first alteration " + message_to_encrypt) ## TESTING PURPOSES

    ## Now, we will shift by 2 in the forward direction
    shiftMethod(message_to_encrypt, 2, alphabets=alphabets)

encryptTheMessage(message_to_encrypt)
encryptedMessage = messageFinal
print("Encrypted Message: " + encryptedMessage)

## Decrypt Method that takes the encrypted string and reverses the encryption
def decryptTheMessage(m2):

    ## Now, we will shift by 24 in the forward direction
    ##shiftMethod(m2, 24, alphabets=alphabets)
    shiftMethod(m2, 24, alphabets=alphabets)
    m2 = messageFinal
    m2 = m2[::-1]
    ## Undo the encryption
    print ("Decrypted Message:: " + message_to_encrypt)

## Takes the encrypted message and throws it into the decrypt method
decryptTheMessage(encryptedMessage)