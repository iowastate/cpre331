## Author;
## Brendon Droege
## CprE/CybE 331
## Lab 02, Part 1, 2022

import string
message_to_decrypt = input('Message to Decrypt: ')
shift_by = int(input('Shift by: '))

alphabets = (string.ascii_lowercase, string.ascii_uppercase, string.digits) ## Stolen from the internet (string library)

def shiftMethod(message, shiftBy, alphabets):
    
    def shiftME(alphabet):
        return alphabet[shiftBy:] + alphabet[:shiftBy] ## How we actually attach the shifted letters together and "shift" them

    ## Takes the alphabet and shifts it by shiftME
    firstShift = tuple(map(shiftME, alphabets)) ## Makes an interable and mutable list

    combineSequence = ''.join(alphabets)
    completeShift = ''.join(firstShift)

    ## maketrans was a stolen premade function that was modified for my evil doings
    ## makes a table of strings that takes the ascii values of the alphabet and the shifted alphabet
    finalTable = str.maketrans(combineSequence, completeShift)

    ## Translate the table of integers into a string that maps the letters together
    print("Shifted Message: " + message.translate(finalTable))
    return message.translate(finalTable)

shiftMethod(message_to_decrypt, shift_by, alphabets=alphabets) ## Calls our method with the user inputted information (message/shift)
