## Author
## Brendon Droege
## CprE/CybE 331
## Lab 03, 2022 (Vigenere Cipher)

import string
import json
import collections

message_to_decrypt = input("Message to Decypt: ") ## User input of the decrypted message

wordList = [] 
freqDict = dict()

## Create a list of all the 3 letter combinations
for i in range(len(message_to_decrypt) - 3): ## -3 to not go over the size
    ## Append our list with the first, second, and third characters at i
    wordList.append([message_to_decrypt[i], message_to_decrypt[i+1], message_to_decrypt[i+2]])

## Adds the 3 letter pairs into the dictionary and keeps track on how frequent they appear 
for trigrams in wordList:
    ## Counting the frequency of the trigrams
    freq = wordList.count(trigrams)
    freqDict[", ".join(trigrams)] = freq

## The print max was stolen from the internet
print ("\nMost Frequent Trigram: " + max(freqDict, key=freqDict.get), freqDict[max(freqDict, key=freqDict.get)])
print ("-------------------------------")

for item in freqDict:
    if freqDict[item] > 4: ## The 4 is a manipulative value that I used to limit the print out of the dictionary to show me which trigram was most frequent
        print("Letters: {} --> Count: {}".format(item,freqDict[item]))

findIndex = input("Which letter would you like to find the index of? ")



def findAllIndex (message, tri, occ): ## finds the trigrams
    
    start = message.find(tri)
    while start >= 0 and occ > 1:
        start = message.find(tri, start+len(tri))
        occ -= 1
    print (start)
    occ = occ + 1
    return start
    
freqCnt = input("What is the count for this trigram? ")
intFreqCnt = int(freqCnt)

while intFreqCnt > 0: ## Prints all the indexes and their frequencies for the length of the frequency count!
    findAllIndex(message=message_to_decrypt, tri=findIndex, occ=intFreqCnt)
    intFreqCnt = intFreqCnt - 1

print()




firstShift = message_to_decrypt[::5] ## Breaks the letters by every 5 starting at 0
print("Most Common Letters and their Frequency: " + str(collections.Counter(firstShift).most_common(3))) ## String collections finds the top 3 common letters
print("First Cipher: " + firstShift + "\n")

secondShift = message_to_decrypt[1::5] ## Breaks the letters by every 5 starting at 1
print("Most Common Letters and their Frequency: " + str(collections.Counter(secondShift).most_common(3))) ## String collections finds the top 3 common letters
print("Second Cipher: " + secondShift + "\n")

thirdShift = message_to_decrypt[2::5] ## Breaks the letters by every 5 starting at 2
print("Most Common Letters and their Frequency: " + str(collections.Counter(thirdShift).most_common(3))) ## String collections finds the top 3 common letters
print("Third Cipher: " + thirdShift + "\n")

fourthShift = message_to_decrypt[3::5] ## Breaks the letters by every 5 starting at 3
print("Most Common Letters and their Frequency: " + str(collections.Counter(fourthShift).most_common(3))) ## String collections finds the top 3 common letters
print("Fourth Cipher: " + fourthShift + "\n")

fifthShift = message_to_decrypt[4::5] ## Breaks the letters by every 5 starting at 4
print("Most Common Letters and their Frequency: " + str(collections.Counter(fifthShift).most_common(3))) ## String collections finds the top 3 common letters
print("Fifth Cipher: " + fifthShift + "\n")

def shiftByN(letter, n):
    print(chr((ord(letter) - 97 + n % 26) % 26 + 97), end='') ## only is completely accurate for lower case

## Print to lower to not ruin the unicode 
firstShift = firstShift.lower()
secondShift = secondShift.lower()
thirdShift = thirdShift.lower()
fourthShift = fourthShift.lower()
fifthShift = fifthShift.lower()

print("\n")

for c in firstShift:
    shiftByN(letter=c, n=0) ## Shift by A "0"

print("\n")

for c in secondShift:
    shiftByN(letter=c, n= -6) ## Shift by G "6", but negative to do a reverse shift

print("\n")

for c in thirdShift:
    shiftByN(letter=c, n= -1) ## Shift by B "1", but negative to do a reverse shift

print("\n")

for c in fourthShift:
    shiftByN(letter=c, n= -21) ## Shift by V "21", but negative to do a reverse shift

print("\n")

for c in fifthShift:
    shiftByN(letter=c, n= -3) ## Shift by D "3", but negative to do a reverse shift

## What you get at the end is the answered encryption!
## Just read down the rows ;)!
## WESET SAILO NTHIS ... yadda yadda