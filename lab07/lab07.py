## Author: Brendon Droege
## CprE/CybE 331, Lab 07, Fall 2022
## 
## 4-Bit Block Fiestal Encoding/Decoding
##

import copy

## Lookup table (recommonded by TA)
def s_box(key_bits, index):

    key_bits = int(key_bits, 2)
    index = int(index, 2)
    box = [[0]*4]*16        # Create List

    # Fill last with predetermined values
    box[0] = [0, 2, 2, 3]
    box[1] = [2, 3, 0, 0]
    box[2] = [1, 2, 0, 0]
    box[3] = [3, 0, 1, 3]
    box[4] = [2, 0, 3, 1]
    box[5] = [1, 3, 2, 2]
    box[6] = [3, 1, 3, 2]
    box[7] = [0, 1, 2, 1]
    box[8] = [3, 1, 0, 2]
    box[9] = [1, 0, 1, 3]
    box[10] = [0, 3, 3, 0]
    box[11] = [1, 2, 3, 1]
    box[12] = [2, 3, 1, 2]
    box[13] = [3, 1, 0, 0]
    box[14] = [0, 0, 2, 1]
    box[15] = [2, 2, 1, 3]
    
    return box[key_bits][index]

# Feistel Encode Function/Method
def encode_func(block_bits, key_bits):
    
    # Round 1
    block_bits = encode_rounds_method(block_bits, key_bits)     ## Method to begin rotatation for the rounds
    key_bits = encode_rotate(key_bits)
    print("Round 1: " + block_bits)
    
    # Round 2
    block_bits = encode_rounds_method(block_bits, key_bits)     ## Method to begin rotatation for the rounds
    key_bits = encode_rotate(key_bits)
    print("Round 2: " + block_bits)
    
    # Round 3 (Final)
    block_bits = encode_rounds_method(block_bits, key_bits)     ## Method to begin rotatation for the rounds
    print("Round 3: " + block_bits)
    
    return (block_bits, key_bits)

# Our main Feistel Encoding Method for Each Round
def encode_rounds_method(block_bits, key_bits):

    left = block_bits[:2]       ## Left bits
    right = block_bits[2:]      ## Right Bits

    s = str(s_box(key_bits, right)) # Uses our lookup table
    tempBits = left ## Hold our left bits
    left = right

    ## Perform operation
    right = int(tempBits, 2) ^ int(s)
    right = str(bin(right)[2:])

    ## Bit protection from either side 
    if (right == "0"):
        right = "00"
    elif (right == "1"):
        right = "01"
        
    block_bits = left + right ## add the left bits with the right
    
    return block_bits


# Take the left bits of the key and shift, our original (first) bit now becomes our last bit
def encode_rotate(key_bits):

    key_bits = list(key_bits)
    tempKey = copy.deepcopy(key_bits)

    ## Create a copy (temp values) of our key bits and store them into a list
    key_bits[0] = tempKey[1]
    key_bits[1] = tempKey[2]
    key_bits[2] = tempKey[3]
    key_bits[3] = tempKey[0]
    
    key_bits = "".join(key_bits)
    return key_bits


# Our main Feistel Encoding Method for Each Round
def decode_func(block_bits, key_bits):

    # Round 1
    block_bits = decode_rounds_method(block_bits, key_bits)
    key_bits = decode_rotate(key_bits)
    print("Round 1: " + block_bits)
    
    # Round 2
    block_bits = decode_rounds_method(block_bits, key_bits)
    key_bits = decode_rotate(key_bits)
    print("Round 2: " + block_bits)
    
    # Round 3
    block_bits = decode_rounds_method(block_bits, key_bits)
    print("Round 3: " + block_bits)


# Feistel decoding round    
def decode_rounds_method(block_bits, key_bits):

    left = block_bits[:2]   ## Left Bits
    right = block_bits[2:]  ## Right Bits

    tempBits = right
    right = left

    s = str(s_box(key_bits, right)) # Uses our S table as mentioned prior

    left = int(tempBits, 2) ^ int(s) ## Updates new key
    left = str(bin(left)[2:])

    ## Bit protection from either side 
    if (left == "0"):
        left = "00"
    elif (left == "1"):
        left = "01"
        
    block_bits = left + right ## put back together
    
    return block_bits


# Take the right bits of the key and shift, our stale (last) bit now becomes our first bit
def decode_rotate(key_bits):

    key_bits = list(key_bits)
    tempKey = copy.deepcopy(key_bits)

    ## Create a copy (temp values) of our key bits and store them into a list
    key_bits[0] = tempKey[3]
    key_bits[1] = tempKey[0]
    key_bits[2] = tempKey[1]
    key_bits[3] = tempKey[2]
    
    key_bits = "".join(key_bits) ## Joins back together
    return key_bits
    
def main():

    ## User Input (Lab had 4-Bit Input of Plaintext [1010] and a 4-Bit Key of [0111])
    block_bits = input("4-bits of Input: ")
    key_bits = input("4-bits Key: ")

    print("\nEncoding:")
    block_bits , key_bits = encode_func(block_bits, key_bits)

    print("\nDecoding:")

    decode_func(block_bits, key_bits)

if __name__ == '__main__':
    main()