## Author: Brendon Droege
## CybE/CprE 331, Lab 06
## Stream Cipher Fun!

import binascii
from os.path import exists


#Returns the most common element in a list 
def mostCommon(lst):
	# IMPLEMENT HERE
    commonElement = max(set(lst), key = lst.count)
    return commonElement

#Used to calculate m[0]
def flaw1(file):
    with open(file, "r") as f:
        for line in f:
            entry = line.strip().split(' ')   	# Entry  	= ['0X01FF00', '0XDB']
            temp_x = "".join(entry[:1]) ## X-Bits Temp
            final_x = temp_x[-2:] ## X-Bits Final
            final_c0 = "".join(entry[1:]) ## C[0] Final
            x_bits_dec = int(final_x, 16)
            x_bits_bin = format(x_bits_dec, 'b')
            c0_dec = int(final_c0, 16)
            c0_bin = format(c0_dec, 'b')
            right_half = (x_bits_dec + 2) % 256
            right_half_bin = format(right_half, 'b')
            xor_result = int(c0_bin,2) ^ int(right_half_bin,2)

          
            xor_to_hex = hex(xor_result)

#           print("X-Bits: " + str(final_x) + " | " + "C[0]: " + str(final_c0) + " > " + "X-Bits in Dec: " + str(x_bits_dec) + " | "+ "X-Bits in Binary: " + str(x_bits_bin) + " | "
#           + " | " + "C[0] in Dec: " + str(c0_dec) + " | " + "C[0] in Binary: " + str(c0_bin) + " | " + "Right Half Binary: " + str(right_half_bin))
#          print("XOR Result (Binary): " + '{0:b}'.format(xor_result) + " || " + "XOR Result (Hex): " + str(xor_to_hex) + "\n")
            
            ## Most Common m[0] = 0x6
            
            most_common = hex(0x6)

    return most_common   	 

arr2 = []

#Used to calculate k[0]
def flaw2(file, m):

    m_dec = int(m, 16)
    m_bin = format(m_dec, 'b')

    with open(file, "r") as f:
        arr = []
     
        for line in f:
            entry = line.strip().split(' ')   	# Entry  	= ['0X01FF00', '0XDB']
            temp_x = "".join(entry[:1]) ## X-Bits Temp
            final_x = temp_x[-2:] ## X-Bits Final
            final_c0 = "".join(entry[1:]) ## C[0] Final
            x_bits_dec = int(final_x, 16)
            c0_dec = int(final_c0, 16)
            c0_bin = format(c0_dec, 'b')
           # print("X-Bits: " + str(final_x) + " | " + "X-Bits (Decimal): " + str(x_bits_dec) + " | " + "X-Bits (Binary): " + str(x_bits_bin) + "\t\t" +    ## TESTING
            #"C[0] Bits: " + str(final_c0) + " | " + "C[0] Bits (Decimal): " + str(c0_dec) + " | " + "C[0] Bits (Binary): " + str(c0_bin))

          
        
            xor_result = int(c0_bin,2) ^ int(m_bin,2)
            xor_result = xor_result - x_bits_dec - 6
           # print(str(xor_result))     ## TESTING

            arr.append(xor_result) # IMPLEMENT HERE

   # print("XOR_RESULT ARRAY: \n" + str(arr)) Testing
    # print("\nMOST COMMON IN ARRAY: \n" + str(hex(max(set(arr), key=arr.count))))  ## 0x63

    most_common = str(hex(max(set(arr), key=arr.count)))

    ## add the most common (k0) to the array of k's
    arr2.append(max(set(arr), key=arr.count))

    return most_common

#Used to calculate k[1], k[2], ... k[12]
def flaw3(file, m, k):
    with open(file, "r") as f:

        m = int(m, 16)
        arr = []
        

        for line in f:
            entry = line.strip().split(' ')   	# Entry  	= ['0X01FF00', '0XDB']
            
           
            a = int (entry[1], 16)
            b = int (entry[0], 16)
          
            ## Easier "case" way to do the process without assigning a loop 
            if k == 1:
                c = b + 10 + arr2[0]
                arr.append(((a ^ m) - c) % 256)
            elif k == 2:
                c = b + 15 + arr2[0] + arr2[1]
                arr.append (((a ^ m) - c) % 256)
            elif k == 3:
                c = b + 21 + arr2[0] + arr2[1] + arr2[2]
                arr.append (((a ^ m) - c) % 256)
            elif k == 4:
                c = b + 28 + arr2[0] + arr2[1] + arr2[2] + arr2[3]
                arr.append (((a ^ m) - c) % 256)
            elif k == 5:
                c = b + 36 + arr2[0] + arr2[1] + arr2[2] + arr2[3] + arr2[4]
                arr.append (((a ^ m) - c) % 256)
            elif k == 6:
                c = b + 45 + arr2[0] + arr2[1] + arr2[2] + arr2[3] + arr2[4] + arr2[5]
                arr.append (((a ^ m) - c) % 256)
            elif k == 7:
                c = b + 55 + arr2[0] + arr2[1] + arr2[2] + arr2[3] + arr2[4] + arr2[5] + arr2[6]
                arr.append (((a ^ m) - c) % 256)
            elif k == 8:
                c = b + 66 + arr2[0] + arr2[1] + arr2[2] + arr2[3] + arr2[4] + arr2[5] + arr2[6] + arr2[7]
                arr.append (((a ^ m) - c) % 256)
            elif k == 9:
                c = b + 78 + arr2[0] + arr2[1] + arr2[2] + arr2[3] + arr2[4] + arr2[5] + arr2[6] + arr2[7] + arr2[8]
                arr.append (((a ^ m) - c) % 256)
            elif k == 10:
                c = b + 91 + arr2[0] + arr2[1] + arr2[2] + arr2[3] + arr2[4] + arr2[5] + arr2[6] + arr2[7] + arr2[8] + arr2[9]
                arr.append (((a ^ m) - c) % 256)
            elif k == 11:
                c = b + 105 + arr2[0] + arr2[1] + arr2[2] + arr2[3] + arr2[4] + arr2[5] + arr2[6] + arr2[7] + arr2[8] + arr2[9] + arr2[10]
                arr.append (((a ^ m) - c) % 256)
            elif k == 12:
                c = b + 120 + arr2[0] + arr2[1] + arr2[2] + arr2[3] + arr2[4] + arr2[5] + arr2[6] + arr2[7] + arr2[8] + arr2[9] + arr2[10] + arr2[11]
                arr.append (((a ^ m) - c) % 256)
                
        arr2.append(mostCommon(arr))

    return mostCommon(arr)
    

def main():
	# IMPLEMENT HERE
    print("Beginning Process: \n")
    file_name = 'bytes_01FFXX.txt'
    file_exists = exists(file_name)
   #print("File Exists: " + str(file_exists) + "\nFile Name: " + file_name)  ## Testing

    ## File Names Provided
    file_name3 = "bytes_04FFXX.txt"
    file_name4 = "bytes_05FFXX.txt"
    file_name5 = "bytes_06FFXX.txt"
    file_name6 = "bytes_07FFXX.txt"
    file_name7 = "bytes_08FFXX.txt"
    file_name8 = "bytes_09FFXX.txt"
    file_name9 = "bytes_0AFFXX.txt"
    file_name10 = "bytes_0BFFXX.txt"
    file_name11 = "bytes_0CFFXX.txt"
    file_name12 = "bytes_0DFFXX.txt"
    file_name13 = "bytes_0EFFXX.txt"
    file_name14 = "bytes_0FFFXX.txt"


    #flaw1(file_name) ## Outputted m[0] = 0x6

    file_name2 = 'bytes_03FFXX.txt'
    file_exists2 = exists(file_name2)
   # print("File Exists: " + str(file_exists2) + "\nFile Name: " + file_name2)

    flaw1_m0 = hex(0x6)
  #  flaw2(file_name2, flaw1_m0)


    ## Run through the methords for the K[n]'s and assign them as the values
    m0 = flaw1(file_name)
    k0 = flaw2(file_name2, flaw1_m0)
    k1 = flaw3(file_name3, flaw1_m0, 1)
    k2 = flaw3(file_name4, flaw1_m0, 2)
    k3 = flaw3(file_name5, flaw1_m0, 3)
    k4 = flaw3(file_name6, flaw1_m0, 4)
    k5 = flaw3(file_name7, flaw1_m0, 5)
    k6 = flaw3(file_name8, flaw1_m0, 6)
    k7 = flaw3(file_name9, flaw1_m0, 7)
    k8 = flaw3(file_name10, flaw1_m0, 8)
    k9 = flaw3(file_name11, flaw1_m0, 9)
    k10 = flaw3(file_name12, flaw1_m0, 10)
    k11 = flaw3(file_name13, flaw1_m0, 11)
    k12 = flaw3(file_name14, flaw1_m0, 12)

    k0 = int(k0, 16)

    ## The output in a good looking format
    print("M[0]: \t" +  str(m0))
    print("K[0]: \t" + str((k0))+ "\tCharacter: " + chr(k0))
    print("K[1]: \t" + str(hex(k1))+ "\tCharacter: " + chr(k1))
    print("K[2]: \t" + str(hex(k2))+ "\tCharacter: " + chr(k2))
    print("K[3]: \t" + str(hex(k3))+ "\tCharacter: " + chr(k3))
    print("K[4]: \t" + str(hex(k4))+ "\tCharacter: " + chr(k4))
    print("K[5]: \t" + str(hex(k5))+ "\tCharacter: " + chr(k5))
    print("K[6]: \t" + str(hex(k6))+ "\tCharacter: " + chr(k6))
    print("K[7]: \t" + str(hex(k7))+ "\tCharacter: " + chr(k7))
    print("K[8]: \t" + str(hex(k8))+ "\tCharacter: " + chr(k8))
    print("K[9]: \t" + str(hex(k9))+ "\tCharacter: " + chr(k9))
    print("K[10]:\t" + str(hex(k10))+ "\tCharacter: " + chr(k10))
    print("K[11]:\t" + str(hex(k11))+ "\tCharacter: " + chr(k11))
    print("K[12]:\t" + str(hex(k12))+ "\tCharacter: " + chr(k12))


    return

if __name__ == '__main__':
	main()
