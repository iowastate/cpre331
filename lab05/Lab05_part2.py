## Author: Brendon Droege
## CprE / CybE 331, Lab05 Part 02
## Sept, 2022

import math

## Given from part 01
def cycle(registers):
    xor_valueue = ((registers & 1) ^ (registers >> 1)) & 1
    registers = (registers >> 1) | (xor_valueue << 3)
    return registers

## Taken from part 01
def makekeyStream(registers, length):
    keyStream = str(registers & 1)

    for x in range(0, length - 1):
        registers = cycle(registers)
        keyStream += str(registers & 1)
    return keyStream

## Taken from part 01
def toBinary(string):
    binArry = []

    for c in string:
        temp = format(ord(c), "08b")
        binArry.append(temp)
    binArry = "".join(binArry)

    return binArry

## Taken from part 01
def toAscii(bits):
    ascii_text = ""
  
    for x in range (0, len(bits), 8):
        letter = bits[x]+ bits[x+1] + bits[x+2] + bits[x+3] + bits[x+4] + bits[x+5] + bits[x+6] + bits[x+7]
        letter = chr(int(letter, 2))
        ascii_text = ascii_text + letter

    return ascii_text

def main():
    encrypted_binary = "01110010100001110000010110100001101001011110100011000011001100110001010101000011110100100010001000011010111100111101000101000010100010110000011110101000111101111110110111001010011000110011111101010100110010000110110100011110111010001001111001011000110000100010010110100101101101011110011011000000001100110011100001001001110011100110110100101100111110111000010101011010100011010000001010101010101100101010100111000110011111010111111001010101110110010011101100011101111101001101000101011101100011010001111010110110101001001010100111000000011111010111111001000111110100100110110100100000101101111000011001011100100011000000110011100100101000001110000011011011011110110111111001010010110101000010100001011000111010101001000001000111100000010000111010101000"
    encrypted_binary = list(encrypted_binary)
    known_message = "General Kenobi"
    known_message = toBinary(known_message)
    print("Encryped Binary Bits :\n" + "".join(encrypted_binary) + "\n")
    print("Known Message to Binary :\n" + known_message + "\n")
    
    xor_binary = ""
    for x in range (0, len(known_message)):
        xor_binary += encrypted_binary[x]
       

    #XOR known plaintext binary with first len(known_messagetext) bits of encrypted_binary
    xor_value = int(known_message, 2) ^ int(xor_binary, 2)
    xor_value = list(str(bin(xor_value)[2:]))
    if (len(xor_value) != len(known_message)):
        lengthDiff = len(known_message) - len(xor_value)
        for x in range (0, lengthDiff):
            xor_value.insert(x, '0')
            
        xor_value = "".join(xor_value)
    print("KeyStream :\n" + xor_value + "\n")
    
    #Find the degree of the period
    period = input("Period: ")
    degree = math.log2(int(period) + 1)
    print("Degree: " + str(degree)[:1] + "\n")
    
    #Input the suspected seed to ge the original keyStream after modifying the bits that need to be XOR'd in the cycle function
    seed = input("Seed: ")
    binarySeed = int(seed, 2)
    keyStream = makekeyStream(binarySeed, len(encrypted_binary))
    print("KeyStream: \n" + keyStream + "\n")
    
    #XOR the encrypted_binary with the keyStream
    encrypted_binary = "".join(encrypted_binary)
    binary_plaintext = int(encrypted_binary, 2) ^ int(keyStream, 2)
    print("Decrypted Binary: \n" + str(bin(binary_plaintext)[2:]) + "\n")
    
    #Convert the plaintext bits to ascii characters
    binary_plaintext = list(str(bin(binary_plaintext)[2:]))
    binary_plaintext.insert(0, '0')
    decrypted_message = toAscii(binary_plaintext)
    print("Decrypted Message: \n" + decrypted_message + "\n")

## Main Function/Body
if __name__=="__main__":
  main()