# Lab 05 Part 01 Skeleton code for LFSR cryptosystem
# Skeleton Code Provided by: Iowa State Univeristy, CybE/CprE 331
# Modifications Provided by: Brendon Droege

from operator import xor
from pydoc import plain
import sys


#The cycle function produces the new bit, shifts the registers to the right by one position,
#and puts the new bit in the 4th bit position.
#As it is written it XORs the first bit and the last bit in the registers
#You may need to select different bits to create the correct xorValue
#However, the shifting of the registers will not need to be modified.
def cycle(registers):
  xorValue = ((registers&1) ^(registers >> 3)) & 1
  registers= (registers >> 1) | (xorValue << 3)
  return registers


#Loop through the length of the plaintext in binary
#Create a keystream from the 4th bit in every cycle
#of the registers.  Don't forget that you must
#include bit 4 of the seed value as the first bit
#in the keyStream.  To get the last bit out of the registers
#variable you can &1 to it.
def makeKeystream(registers, length):

#Use cycle(registers) as well as other code you

  keyStream = str(registers & 1) ## Logical bitwise AND operator

  for x in range(0, length - 1):
    registers = cycle(registers)
    keyStream += str(registers & 1)

  return keyStream

#Use to convert a string to binary representation
def toBinary(string):

  binArry = []                                ## Create an empty array for our new binary text

  for i in string:
        temp = format(ord(i), "08b")          ## Format it properly
        binArry.append(temp)                  ## Add to it
  
  binArry = "".join(binArry)

  return binArry

#Use to convert a binary string to ascii
def toAscii(bits):

  newAscii = ""

  for i in range (0, len(bits), 8):
    letter = bits[i] + bits[i + 1] + bits[i + 2] + bits[i + 3] + bits[i + 4] + bits[i + 5] + bits[i + 6] + bits[i + 7] ## To account for the 8 bits
    letter = chr(int(letter, 2))    ## Properly formatting with the int(letter, 2)
    newAscii = newAscii + letter

  return newAscii



def main():
  #Prompt for seed values: input()
  #Wise students will use the int() function with the base 2
  #For example, int('11111111',2) where the 1s are input from the terminal

    seed = input("Enter the seed: ")
    binSeed = int(seed, 2)
    ## print("Binary: " + str(seed)) ## Tester Print Statement

  #Input the plaintext message

    plaintext_message = "Remember, A Jedi's Strength Flows From The Force. But Beware: Anger, Fear, Aggression. The Dark Side, Are They."
    print("Plaintext Message: \n" + plaintext_message + "\n")

  #Convert plaintext message to binary
  #Use the toBinary function

    binary_message = toBinary(plaintext_message)
    print("Binary Message: \n" + str(binary_message) + "\n")


  #Generate a keystream that is the length of our plaintext
  #Use makeKeystream(registers, length)

    keyStream = makeKeystream(binSeed, len(binary_message))
    print("KeyStream: \n" + keyStream + "\n")


  #XOR the Plaintext bits and the Keystream bits
  #Use int(variable,2) to represent the variable in binary value
  #The ^ is the XOR operator in python

    encrypted_message = int(binary_message, 2) ^ int (keyStream, 2)
    print("Encrypted Message: \n" + str(bin(encrypted_message)[2:]) + "\n")

  #To check your work XOR the Ciphertext and the keystream
  #Use int(variable,2) to represent the variable in binary value
  #The ^ is the XOR operator in python

    decrypted_message = encrypted_message ^ int(keyStream, 2)
    print("Decrypted Message: \n" + "0" + str(bin(decrypted_message)[2:]))

    decrypted_message = list(str(bin(decrypted_message)[2:]))
    decrypted_message.insert(0, '0')                            ## Account for the mismatch length

  #To further check your work convert the bits to ascii using
  #the toAscii() function.

    bitsToAscii = toAscii(decrypted_message)
    print("\nAscii Message: \n" + bitsToAscii)

if __name__=="__main__" :

        main()